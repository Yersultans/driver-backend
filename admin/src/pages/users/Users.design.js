import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker
} from 'antd'
import { PlusOutlined, SearchOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const Users = ({ users, addUserHandler }) => {
  const [form] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchNickname, setSearchNickname] = React.useState(null)
  const [avatarUrl, setAvatarUrl] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listUsers, setListUsers] = React.useState(null)

  React.useEffect(() => {
    const descriptiveUsers = users.map(user => {
      return {
        ...user,
        fullname: `${user?.lastname} ${user?.firstname}`,
        genderName:
          (user.sex === 'male' && 'Male') || (user.sex === 'female' && 'Female')
      }
    })
    let searchArray = descriptiveUsers

    if (searchNickname) {
      searchArray = searchArray.filter(user => {
        return (
          user?.username?.toLowerCase().includes(searchNickname) ||
          user.lastname?.toLowerCase().includes(searchNickname) ||
          user.phoneNumber?.toLowerCase().includes(searchNickname) ||
          user.firstname?.toLowerCase().includes(searchNickname)
        )
      })
    }

    setListUsers(searchArray)
  }, [users, searchNickname])

  const columns = [
    {
      title: 'Avatar',
      dataIndex: 'avatarUrl',
      render: item => {
        return <StyledImg src={item} alt="No Icon" />
      }
    },
    {
      title: 'Fullname',
      dataIndex: 'fullname',
      width: '25%'
    },
    {
      title: 'Email',
      dataIndex: 'username',
      width: '15%'
    },
    {
      title: 'phoneNumber',
      dataIndex: 'phoneNumber',
      width: '15%'
    },
    {
      title: 'gender',
      dataIndex: 'genderName',
      width: '10%'
    },
    {
      title: 'Action',
      width: '20%',
      render: (text, item) => (
        <span>
          <Link to={`/users/${item.id}`}>Edit</Link>
        </span>
      )
    }
  ]

  const handleCreate = values => {
    console.log('values', values)
    addUserHandler(values)
    setModalVisible(false)
    setAvatarUrl(null)
  }

  return (
    <>
      <Table
        dataSource={listUsers}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Firstname/Lastname/Email/Phone Number"
              onChange={e => {
                setSearchNickname(e.target.value.toLowerCase())
              }}
            />

            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> New User
            </Button>
          </StyledHeaderContainer>
        )}
      />
      <Modal
        visible={modalVisible}
        title="New User"
        okText="Create"
        cancelText="Cancel"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              delete values.avatarUrl

              handleCreate({ ...values, avatarUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="username"
            label={getTooltip('User Email', 'User Email')}
            name="username"
            rules={[
              {
                required: true,
                message: `Please enter the user's email`
              }
            ]}
          >
            <Input placeholder="Email" />
          </FormItem>
          <FormItem
            key="lastname"
            label={getTooltip(`user's Lastname`, `user's Lastname`)}
            name="lastname"
            rules={[
              {
                required: true,
                message: `Please enter the user's Lastname`
              }
            ]}
          >
            <Input placeholder="Lastname" />
          </FormItem>
          <FormItem
            key="firstname"
            label={getTooltip(`user's Firstname`, `ser's Firstname`)}
            name="firstname"
            rules={[
              {
                required: true,
                message: `Please enter the user's Firstname`
              }
            ]}
          >
            <Input placeholder="Firstname" />
          </FormItem>
          <FormItem
            key="phoneNumber"
            label={getTooltip(`user's PhoneNumber`, `user's PhoneNumber`)}
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: `Please enter the user's PhoneNumber`
              }
            ]}
          >
            <Input placeholder="PhoneNumber" />
          </FormItem>
          <FormItem
            key="gender"
            label={getTooltip(`User's Gender`, `User's Gender`)}
            name="gender"
            rules={[
              {
                required: true,
                message: `Please enter the user's PhoneNumber`
              }
            ]}
          >
            <Select mode="single" placeholder="select Gender" showSearch>
              <Select.Option key="male" value="male">
                Male
              </Select.Option>
              <Select.Option key="female" value="female">
                Female
              </Select.Option>
            </Select>
          </FormItem>
          <FormItem
            key="birthday"
            label={getTooltip(`User's Birthday`, `User's Birthday`)}
            name="birthday"
            rules={[
              {
                required: true,
                message: `Please enter the user's Birthday`
              }
            ]}
          >
            <StyledDatePicker placeholder="select Date" />
          </FormItem>
          <FormItem
            key="role"
            label={getTooltip(`User's Role`, `User's Role`)}
            name="role"
            rules={[
              {
                required: true,
                message: `Please enter the user's Role`
              }
            ]}
          >
            <Select mode="single" placeholder="select Role" showSearch>
              <Select.Option key="admin" value="admin">
                Admin
              </Select.Option>
              <Select.Option key="user" value="user">
                Client
              </Select.Option>
              <Select.Option key="driver" value="driver">
                Driver
              </Select.Option>
            </Select>
          </FormItem>
          <FormItem
            key="password"
            label={getTooltip(`User's Password`, `User's Password`)}
            name="password"
            rules={[
              {
                required: true,
                message: `Please enter the user's Password`
              }
            ]}
          >
            <Input placeholder="Password" />
          </FormItem>
          <FormItem
            key="avatarUrl"
            label={getTooltip(`User's Avatar`, `User's Avatar`)}
            name="avatarUrl"
            rules={[
              {
                required: false,
                message: `Please enter the user's Avatar`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setAvatarUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={avatarUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
    </>
  )
}

Users.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  addUserHandler: PropTypes.func.isRequired
}

export default Users
