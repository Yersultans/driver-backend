import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../context/useLoading'
import Users from './Users.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'

const GET_USERS = gql`
  query getUsers {
    users {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
    }
  }
`

const ADD_USER = gql`
  mutation addUser($input: UserInput) {
    addUser(input: $input) {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
    }
  }
`

const UsersContainer = () => {
  const { showLoading, hideLoading } = useLoading()
  const [allUsers, setAllUsers] = React.useState([])
  const { data, loading, error, refetch } = useQuery(GET_USERS)
  const [addUser, { error: errorAddUser }] = useMutation(ADD_USER, {
    update(cache, { data: { addUser: addUserItem } }) {
      const { users } = cache.readQuery({ query: GET_USERS })
      cache.writeQuery({
        query: GET_USERS,
        data: { users: users.concat([addUserItem]) }
      })
      hideLoading()
      toast.success('User Added')
    }
  })

  React.useEffect(() => {
    if (errorAddUser) {
      console.log(errorAddUser)
    }
  }, [errorAddUser])

  React.useEffect(() => {
    if (data && !error && !loading) {
      setAllUsers(data.users)
    }
  }, [data, loading, error])

  const addUserHandler = value => {
    addUser({
      variables: {
        input: value
      }
    })
  }

  if (loading) {
    return <Loading />
  }

  return (
    <Users
      users={allUsers}
      addUser={addUser}
      refetch={refetch}
      addUserHandler={addUserHandler}
    />
  )
}

export default WithMainLayout(UsersContainer)
