import React from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Modal,
  Form,
  Input,
  Select,
  Switch,
  Avatar,
  DatePicker
} from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'
import moment from 'moment'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditUser = ({
  visible,
  onCancel,
  title,
  editUser,
  onUpdate,
  onDelete,
  avatarUrl,
  setAvatarUrl
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editUser.id)
            })
          }}
        >
          Delete
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Cancel
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                delete values.avatarUrl
                onUpdate(editUser.id, { ...values, avatarUrl })
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="username"
          label={getTooltip('Email of User', 'Email of User')}
          name="username"
          rules={[
            {
              required: true,
              message: `Please enter the user's email`
            }
          ]}
          initialValue={editUser.username}
        >
          <Input placeholder="Email" />
        </FormItem>
        <FormItem
          key="lastname"
          label={getTooltip(`User's Lastname`, `User's Lastname`)}
          name="lastname"
          rules={[
            {
              required: true,
              message: `Please enter the user's lastname`
            }
          ]}
          initialValue={editUser.lastname}
        >
          <Input placeholder="Lastname" />
        </FormItem>
        <FormItem
          key="firstname"
          label={getTooltip(`User's Firstname`, `User's Firstname`)}
          name="firstname"
          rules={[
            {
              required: true,
              message: `Please enter the user's Firstname`
            }
          ]}
          initialValue={editUser.firstname}
        >
          <Input placeholder="Firstname" />
        </FormItem>
        <FormItem
          key="phoneNumber"
          label={getTooltip(`User's Phone Number`, `User's Phone Number`)}
          name="phoneNumber"
          rules={[
            {
              required: true,
              message: `Please enter the user's Phone Number`
            }
          ]}
          initialValue={editUser.phoneNumber}
        >
          <Input placeholder="Phone Number" />
        </FormItem>
        <FormItem
          key="birthday"
          label={getTooltip(`User's Birthday`, `User's Birthday`)}
          name="birthday"
          rules={[
            {
              required: true,
              message: `Please enter the user's Birthday`
            }
          ]}
          initialValue={moment(editUser.birthday)}
        >
          <StyledDatePicker placeholder="Select Date" />
        </FormItem>
        <FormItem
          key="gender"
          label={getTooltip('Gender', `User's Gender`)}
          name="gender"
          rules={[
            {
              required: true,
              message: `Please enter the user's Gender`
            }
          ]}
          initialValue={editUser.gender}
        >
          <Select mode="single" placeholder="select Gender" showSearch>
            <Select.Option key="male" value="male">
              Male
            </Select.Option>
            <Select.Option key="female" value="female">
              Female
            </Select.Option>
          </Select>
        </FormItem>
        <FormItem
          key="role"
          label={getTooltip('Role', `User's Role`)}
          name="role"
          rules={[
            {
              required: true,
              message: `Please enter the user's Role`
            }
          ]}
          initialValue={editUser.role}
        >
          <Select mode="single" placeholder="select Role" showSearch>
            <Select.Option key="admin" value="admin">
              Admin
            </Select.Option>
            <Select.Option key="user" value="user">
              Client
            </Select.Option>
            <Select.Option key="driver" value="driver">
              Driver
            </Select.Option>
          </Select>
        </FormItem>
        <FormItem
          key="password"
          label={getTooltip(`User's Password`, `User's Password`)}
          name="password"
          rules={[
            {
              required: true,
              message: `Please enter the user's Password`
            }
          ]}
        >
          <Input placeholder="Password" />
        </FormItem>
        <FormItem
          key="avatarUrl"
          label={getTooltip('Avatar', `User's Avatar`)}
          name="avatarUrl"
          rules={[
            {
              required: false,
              message: `Please enter the user's Avatar`
            }
          ]}
        >
          <ImageUploadContainer>
            <ImageUpload
              onUpdate={value => {
                setAvatarUrl(value)
              }}
            />
            <StyledAvatar
              size="96"
              shape="square"
              src={avatarUrl || editUser.avatarUrl}
            />
          </ImageUploadContainer>
        </FormItem>
        <FormItem
          key="isBlocked"
          label={getTooltip('Blocked?', 'Blocked user?')}
          name="isBlocked"
          valuePropName="isBlocked"
        >
          <Switch
            defaultChecked={editUser?.isBlocked}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        </FormItem>
      </Form>
    </Modal>
  )
}

EditUser.propTypes = {
  editUser: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditUser
