import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'

import User from './User.design'
import Loading from '../shared/Loading'
import { useLoading } from '../../context/useLoading'
import withMainLayout from '../../hocs/withMainLayout'

const GET_DATA = gql`
  query getData($id: ID!) {
    user(id: $id) {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
      isBlocked
    }
  }
`
const GET_USERS = gql`
  query getUsers {
    users {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
      isBlocked
    }
  }
`

const UPDATE_USER = gql`
  mutation updateUser($id: ID!, $input: UserInput) {
    updateUser(id: $id, input: $input) {
      id
      username
      lastname
      firstname
      birthday
      role
      phoneNumber
      avatarUrl
      gender
      isBlocked
    }
  }
`
const DELETE_USER = gql`
  mutation deleteUser($id: ID!) {
    deleteUser(id: $id)
  }
`

const UserContainer = props => {
  const { id: userId } = props.match.params
  const history = useHistory()
  const [userData, setUserData] = React.useState(null)
  const { showLoading, hideLoading } = useLoading()

  const { data, loading, error } = useQuery(GET_DATA, {
    variables: { id: userId }
  })
  const [updateUser] = useMutation(UPDATE_USER)

  const [deleteUser] = useMutation(DELETE_USER)

  useEffect(() => {
    if (data && data.user) {
      setUserData(data.user)
    } else if (error) {
      console.log('error', error)
    }
  }, [data, loading, error])

  if (loading && !userData)
    return (
      <div>
        <Loading />
      </div>
    )

  const handleUpdateClick = (userId, values) => {
    toast.success('User successfully edited')
    updateUser({ variables: { id: userId, input: values } })
  }

  const handleDeleteClick = userId => {
    showLoading()
    deleteUser({ variables: { id: userId } })
    hideLoading()
    toast.success('User deleted successfully')
    history.push('/users')
  }
  return (
    <>
      {userData && (
        <User
          user={userData}
          updateClick={handleUpdateClick}
          deleteClick={handleDeleteClick}
        />
      )}
    </>
  )
}

UserContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }).isRequired
}

export default withMainLayout(UserContainer)
