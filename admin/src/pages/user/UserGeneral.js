import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Form, Button, Modal, Descriptions, Input, Avatar, Switch } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import moment from 'moment'
import 'moment/locale/ru'

import { useAuth } from '../../context/useAuth'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'
import EditUser from './EditUser'

const FormItem = Form.Item

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const StyledButton = styled(Button)`
  width: 320px;
  margin-top: 14px;
`
const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`
const FeilDiv = styled.div`
  color: red;
`

const UserGeneral = ({ user, updateClick, deleteClick }) => {
  const editors = ['admin']
  const [form] = Form.useForm()
  const [editModal, setEditModal] = React.useState(false)
  const [avatarUrl, setAvatarUrl] = React.useState(user.avatarUrl)
  const { getRole } = useAuth()
  const role = getRole()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const renderField = (field, index) => {
    const backgroundColor = index % 2 === 0 ? '#fafafa' : '#ebebeb'
    const labelStyle = { width: '200px', backgroundColor }
    if (typeof user[field.value] === 'boolean') {
      return (
        <Descriptions.Item
          label={field.label}
          labelStyle={labelStyle}
          key={field.label}
        >
          {user[field.value] === false ? 'No' : 'Yes'}
        </Descriptions.Item>
      )
    }
    if (field.isImage) {
      return (
        <Descriptions.Item label={field.label} key={field.label}>
          {user[field.value] ? (
            <StyledImg src={user[field.value]} alt="No Icon" />
          ) : (
            <FeilDiv>Фото Нет</FeilDiv>
          )}
        </Descriptions.Item>
      )
    }

    if (field.isDate) {
      return (
        <Descriptions.Item label={field.label} key={field.label}>
          {user[field.value] ? (
            moment(user[field.value]).format('DD-MM-YYYY')
          ) : (
            <FeilDiv>Not specified</FeilDiv>
          )}
        </Descriptions.Item>
      )
    }

    if (field.isGender) {
      return (
        <Descriptions.Item label={field.label} key={field.label}>
          {user[field.value] ? (
            user[field.value] === 'male' ? (
              'Male'
            ) : (
              'Female'
            )
          ) : (
            <FeilDiv>Not specified</FeilDiv>
          )}
        </Descriptions.Item>
      )
    }

    return (
      <Descriptions.Item
        label={field.label}
        labelStyle={labelStyle}
        key={field.label}
      >
        {user[field.value]}
      </Descriptions.Item>
    )
  }

  const fields = [
    { value: 'avatarUrl', label: 'Photo', isImage: true },
    { value: 'username', label: 'Email' },
    { value: 'lastname', label: 'LastName' },
    { value: 'firstname', label: 'FirstName' },
    { value: 'phoneNumber', label: 'Phone Number' },
    { value: 'role', label: 'Role' },
    { value: 'birthday', label: 'Birthday', isDate: true },
    { value: 'gender', label: 'Gender', isGender: true },
    { value: 'isBlocked', label: 'Blocked User?' }
  ]

  const handleUpdate = (id, values) => {
    updateClick(id, {
      ...values,
      isBlocked:
        typeof values.isBlocked !== 'undefined'
          ? values.isBlocked
          : user.isBlocked
    })

    setEditModal(false)
  }

  const handleDelete = id => {
    deleteClick(id)
    setEditModal(false)
  }

  return (
    <MainContainer>
      <ButtonContainer>
        <h2> General </h2>
        {editors.includes(role) && (
          <Button type="primary" onClick={() => setEditModal(true)}>
            Edit
          </Button>
        )}
      </ButtonContainer>
      <Descriptions title="User Info" bordered column={1} size="small">
        {fields.map((field, index) => {
          return renderField(field, index)
        })}
      </Descriptions>
      {editModal && (
        <EditUser
          visible={editModal}
          onCancel={() => {
            setAvatarUrl(null)
            setEditModal(null)
          }}
          title="Edit User"
          editUser={user}
          onUpdate={handleUpdate}
          onDelete={handleDelete}
          avatarUrl={avatarUrl}
          setAvatarUrl={setAvatarUrl}
        />
      )}
    </MainContainer>
  )
}

UserGeneral.propTypes = {
  user: PropTypes.shape({}).isRequired,
  updateClick: PropTypes.func.isRequired
}

export default UserGeneral
