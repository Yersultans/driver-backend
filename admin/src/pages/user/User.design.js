import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Layout, Menu, Breadcrumb, Button, Select, Form, Input } from 'antd'
import { Link } from 'react-router-dom'
import { InfoCircleOutlined, GatewayOutlined } from '@ant-design/icons'
import EmailEditor from 'react-email-editor'

import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

import UserGeneral from './UserGeneral'
import UserCarsContainer from '../userCars/UserCars.container'

const FormItem = Form.Item

const { Header, Sider, Content } = Layout

const StyledBreadcrumb = styled(Breadcrumb)`
  margin: 16px 0;
`

const StyledHeader = styled(Header)`
  background: #fff;
  border-bottom: 1px solid #d9d9d9;
  padding: 0px 24px;
`

const StyledMenu = styled(Menu)`
  height: 100%;
  padding-top: 16px;
`
const StyledLayout = styled(Layout)`
  padding: 0 24px 24px;
  background: #fff;
`

const StyledContent = styled(Content)`
  padding: 24px;
  margin: 0px;
  min-height: 280px;
`

const User = ({ user, updateClick, deleteClick }) => {
  const [currentTab, setCurrentTab] = useState(1)

  const handleTabChange = activeKey => {
    setCurrentTab(activeKey)
  }

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Layout>
      <StyledBreadcrumb>
        <Breadcrumb.Item>
          <Link to="/users">Users</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>{user?.title}</Breadcrumb.Item>
      </StyledBreadcrumb>
      <StyledHeader>
        <h2>Edit User</h2>
      </StyledHeader>
      <Layout>
        <Sider>
          <StyledMenu mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item
              icon={<InfoCircleOutlined />}
              key="1"
              onClick={() => handleTabChange(1)}
            >
              General
            </Menu.Item>
            <Menu.Item
              icon={<GatewayOutlined />}
              key="2"
              onClick={() => handleTabChange(2)}
            >
              Cars
            </Menu.Item>
          </StyledMenu>
        </Sider>
        <StyledLayout>
          <StyledContent className="site-layout-background">
            {currentTab === 1 && (
              <UserGeneral
                user={user}
                updateClick={updateClick}
                deleteClick={deleteClick}
              />
            )}
            {currentTab === 2 && <UserCarsContainer userId={user.id} />}
          </StyledContent>
        </StyledLayout>
      </Layout>
    </Layout>
  )
}
User.propTypes = {
  user: PropTypes.shape({}).isRequired,
  updateClick: PropTypes.func.isRequired,
  deleteClick: PropTypes.func.isRequired
}
export default User
