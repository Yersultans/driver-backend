import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css'
import { ToastContainer } from 'react-toastify'
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  ApolloLink,
  concat
} from '@apollo/client'
import 'typeface-roboto'

import './App.css'
import Login from './auth/Login'
import Users from './users/Users.container'
import User from './user/User.container'


import CarModels from './userCars/UserCars.container'

import PublicPage from './PublicPage'

import withHelmet from '../hocs/withHelmet'
import { ProvideAuth } from '../context/useAuth'
import { ProvideLoading } from '../context/useLoading'
import LoadingDialog from '../context/LoadingDialog'
import PrivateRoute from '../hocs/PrivateRoute'

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_GRAPHQL_URL
})

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      authorization: localStorage.getItem('token')
        ? `Bearer ${localStorage.getItem('token')}`
        : null
    }
  })

  return forward(operation)
})

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink)
})

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <ProvideAuth>
        <ProvideLoading>
          <BrowserRouter>
            <ToastContainer />
            <LoadingDialog />
            <Route exact path="/public" component={PublicPage} />
            <Route exact path="/" component={PublicPage} />
            <Route exact path="/login" component={Login} />
            <PrivateRoute
              exact
              path="/users"
              component={Users}
              roles={['admin']}
            />
            <PrivateRoute
              exact
              path="/users/:id"
              component={User}
              roles={['admin']}
            />
          </BrowserRouter>
        </ProvideLoading>
      </ProvideAuth>
    </ApolloProvider>
  )
}

const EnhancedApp = withHelmet([{ tag: 'title', content: 'Admin | Hero-app' }])(
  App
)

export default EnhancedApp
