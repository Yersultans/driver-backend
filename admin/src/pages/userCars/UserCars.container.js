import React from 'react'
import { gql, useMutation, useQuery } from '@apollo/client'
import { toast } from 'react-toastify'

import { useLoading } from '../../context/useLoading'
import UserCars from './UserCars.design'
import WithMainLayout from '../../hocs/withMainLayout'
import Loading from '../shared/Loading'

const GET_USER_CARS = gql`
  query userCarsByUser($userId: ID) {
    userCarsByUser(userId: $userId) {
      id
      name
      imageUrl
      brand
      model
      year
      type
      capacity
    }
  }
`

const ADD_USER_CAR = gql`
  mutation addUserCar($input: UserCarInput) {
    addUserCar(input: $input) {
      id
      name
      imageUrl
      brand
      model
      year
      type
      capacity
    }
  }
`

const DELETE_USER_CAR = gql`
  mutation deleteUserCar($id: ID!) {
    deleteUserCar(id: $id)
  }
`
const UPDATE_USER_CAR = gql`
  mutation updateUserCar($id: ID!, $input: UserCarInput) {
    updateUserCar(id: $id, input: $input) {
      id
      name
      imageUrl
      brand
      model
      year
      type
      capacity
    }
  }
`

const UserCarsContainer = ({ userId }) => {
  const { showLoading, hideLoading } = useLoading()
  const [allUserCars, setAllUserCars] = React.useState([])
  const { data, loading, error, refetch } = useQuery(GET_USER_CARS, {
    variables: {
      userId: userId
    }
  })
  const [updateUserCar] = useMutation(UPDATE_USER_CAR)
  const [addUserCar, { error: errorAddUserCar }] = useMutation(ADD_USER_CAR, {
    update(cache, { data: { addUserCar: addUserCarItem } }) {
      const { userCarsByUser } = cache.readQuery({
        query: GET_USER_CARS,
        variables: {
          userId: userId
        }
      })
      cache.writeQuery({
        query: GET_USER_CARS,
        variables: {
          userId: userId
        },
        data: { userCarsByUser: userCarsByUser.concat([addUserCarItem]) }
      })
      hideLoading()
      toast.success('User added')
    }
  })

  const [deleteUserCar] = useMutation(DELETE_USER_CAR, {
    update(cache, { data: { deleteUserCar: id } }) {
      const { userCarsByUser } = cache.readQuery({
        query: GET_USER_CARS,
        variables: {
          userId: userId
        }
      })
      cache.writeQuery({
        query: GET_USER_CARS,
        variables: {
          userId: userId
        },
        data: {
          userCarsByUser: userCarsByUser.filter(user => user.id !== id)
        }
      })
      hideLoading()
      toast.success('Car deleted successfully')
    }
  })

  React.useEffect(() => {
    if (errorAddUserCar) {
      console.log(errorAddUserCar)
    }
  }, [errorAddUserCar])

  React.useEffect(() => {
    if (data && !error && !loading) {
      setAllUserCars(data.userCarsByUser)
    }
  }, [data, loading, error])

  const addUserCarHandler = value => {
    addUserCar({
      variables: {
        input: { ...value, user: userId }
      }
    })
  }

  const updateUserCarHandler = (id, values) => {
    toast.success('Car successfully edited')
    updateUserCar({ variables: { id, input: values } })
  }

  const deleteUserCarHandler = id => {
    showLoading()
    deleteUserCar({ variables: { id: id } })
    hideLoading()
    toast.success('Car deleted successfully')
  }

  if (loading) {
    return <Loading />
  }

  return (
    <UserCars
      userCars={allUserCars}
      addUserCarHandler={addUserCarHandler}
      updateUserCarHandler={updateUserCarHandler}
      deleteUserCarHandler={deleteUserCarHandler}
    />
  )
}

export default UserCarsContainer
