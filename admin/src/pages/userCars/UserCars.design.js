import React from 'react'
import {
  Table,
  Button,
  Form,
  Modal,
  Input,
  Select,
  Avatar,
  DatePicker,
  Switch,
  Divider
} from 'antd'
import {
  PlusOutlined,
  SearchOutlined,
  CloseOutlined,
  CheckOutlined
} from '@ant-design/icons'
import { Link } from 'react-router-dom'

import styled from 'styled-components'

import PropTypes from 'prop-types'

import EditUserCar from './EditUserCar'
import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'
import cars from '../../utils/cars'

const FormItem = Form.Item

const StyledDatePicker = styled(DatePicker)`
  width: 100%;
`

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const StyledHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const StyledInput = styled(Input)`
  width: 256px;
`

const StyledImg = styled.img`
  width: 50px;
  height: 50px;
`

const FeilDiv = styled.div`
  color: red;
`

const SuccessDiv = styled.div`
  color: green;
`
const UserCars = ({
  userCars,
  addUserCarHandler,
  updateUserCarHandler,
  deleteUserCarHandler
}) => {
  const [form] = Form.useForm()
  const [formForEdit] = Form.useForm()
  const [modalVisible, setModalVisible] = React.useState(false)
  const [searchName, setSearchName] = React.useState(null)
  const [imageUrl, setImageUrl] = React.useState(null)
  const [display, setDisplay] = React.useState('default')
  const [editUserCar, setEditUserCar] = React.useState(null)
  const [selectedBrand, setSelectedBrand] = React.useState(null)

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  const [listUserCars, setListUserCars] = React.useState([])

  React.useEffect(() => {
    if (userCars.length > 0) {
      const descriptiveUserCars = userCars.map(userCar => {
        return {
          ...userCar
        }
      })
      let searchArray = descriptiveUserCars

      if (searchName) {
        searchArray = searchArray.filter(userCar => {
          return userCar.name?.toLowerCase().includes(searchName)
        })
      }
      setListUserCars(searchArray)
    }
  }, [userCars, searchName])

  const columns = [
    {
      title: 'Фото',
      dataIndex: 'imageUrl',
      width: '15%',
      render: item => {
        return item ? (
          <StyledImg src={item} alt="No Icon" />
        ) : (
          <FeilDiv>Нет Фото</FeilDiv>
        )
      }
    },
    {
      title: 'Name',
      dataIndex: 'name',
      width: '15%'
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
      width: '15%'
    },
    {
      title: 'Model',
      dataIndex: 'model',
      width: '15%'
    },
    {
      title: 'Year',
      dataIndex: 'year',
      width: '15%'
    },
    {
      title: 'Name',
      dataIndex: 'name',
      width: '25%'
    },
    {
      title: 'Action',
      width: '30%',
      render: (text, item) => (
        <span>
          <Button
            type="link"
            onClick={() => {
              setDisplay('edit')
              setEditUserCar(null)
              setEditUserCar(
                userCars.find(
                  userCar => userCar.id.toString() === item.id.toString()
                )
              )
              setImageUrl(item.imageUrl)
              console.log(
                'item',
                cars.find(car => car.brand === item.brand)
              )
              setSelectedBrand(cars.find(car => car.brand === item.brand))
            }}
          >
            Edit
          </Button>
          <Divider type="vertical" />
        </span>
      )
    }
  ]

  const handleCreate = values => {
    addUserCarHandler({
      ...values,
      capacity: parseInt(values.capacity),
      year: parseInt(values.year)
    })
    setSelectedBrand(null)
    setModalVisible(false)
    setImageUrl(null)
  }

  const handleUpdate = (id, values) => {
    updateUserCarHandler(id, {
      ...values,
      isActive:
        typeof values.isActive !== 'undefined'
          ? values.isActive
          : editUserCar.isActive
    })
    setSelectedBrand(null)
    setImageUrl(null)
    setDisplay('default')
    setEditUserCar(null)
  }

  const handleDelete = id => {
    console.log('id', id)
    setImageUrl(null)
    setDisplay('default')
    setEditUserCar(null)
    deleteUserCarHandler(id)
  }

  return (
    <>
      <Table
        dataSource={listUserCars}
        columns={columns}
        rowKey={item => item.id}
        title={() => (
          <StyledHeaderContainer>
            <StyledInput
              prefix={<SearchOutlined />}
              placeholder="Name"
              onChange={e => {
                setSearchName(e.target.value.toLowerCase())
              }}
            />

            <Button type="primary" onClick={() => setModalVisible(true)}>
              <PlusOutlined /> New Car
            </Button>
          </StyledHeaderContainer>
        )}
      />

      <Modal
        visible={modalVisible}
        title="New Car"
        okText="Create"
        cancelText="Cancel"
        key="create"
        onCancel={() => {
          setModalVisible(false)
        }}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields()

              delete values.imageUrl

              handleCreate({ ...values, imageUrl })
            })
            .catch(info => {
              // eslint-disable-next-line no-console
              console.log('Validate Failed:', info)
            })
        }}
      >
        <Form form={form} layout="vertical" hideRequiredMark>
          <FormItem
            key="name"
            label={getTooltip('Car Name', 'Car Name')}
            name="name"
            rules={[
              {
                required: true,
                message: `Please write the Car Name`
              }
            ]}
          >
            <Input placeholder="Name" />
          </FormItem>
          <FormItem
            key="brand"
            label={getTooltip('Brand', 'Car Brand')}
            name="brand"
            rules={[
              {
                required: true,
                message: `Please write the Car Brand`
              }
            ]}
          >
            <Select
              mode="single"
              placeholder="select Brand"
              showSearch
              onChange={value =>
                setSelectedBrand(cars.find(car => car.brand === value))
              }
            >
              {cars.map(car => (
                <Select.Option key={car.brand} value={car.brand}>
                  {car.brand}
                </Select.Option>
              ))}
            </Select>
          </FormItem>
          <FormItem
            key="model"
            label={getTooltip('Model', 'Car model')}
            name="model"
            rules={[
              {
                required: true,
                message: `Please write the Car model`
              }
            ]}
          >
            <Select
              mode="single"
              placeholder="select model"
              showSearch
              disabled={!selectedBrand}
            >
              {selectedBrand &&
                selectedBrand?.models.map(model => (
                  <Select.Option key={model} value={model}>
                    {model}
                  </Select.Option>
                ))}
            </Select>
          </FormItem>
          <FormItem
            key="year"
            label={getTooltip('Car year', 'Car year')}
            name="year"
            rules={[
              {
                required: true,
                message: `Please write the Car year`
              }
            ]}
          >
            <Input type="number" placeholder="year" />
          </FormItem>
          <FormItem
            key="capacity"
            label={getTooltip('Car capacity', 'Car capacity')}
            name="capacity"
            rules={[
              {
                required: true,
                message: `Please write the Car capacity`
              }
            ]}
          >
            <Input type="number" placeholder="capacity" />
          </FormItem>
          <FormItem
            key="type"
            label={getTooltip('Type ', 'Car type')}
            name="type"
            rules={[
              {
                required: true,
                message: `Please write the Car type`
              }
            ]}
          >
            <Select mode="single" placeholder="select type" showSearch>
              <Select.Option key="A1" value="A1">
                A1
              </Select.Option>
              <Select.Option key="A" value="A">
                A
              </Select.Option>
              <Select.Option key="B1" value="B1">
                B1
              </Select.Option>
              <Select.Option key="B" value="B">
                B
              </Select.Option>
              <Select.Option key="C1" value="C1">
                C1
              </Select.Option>
              <Select.Option key="C" value="C">
                C
              </Select.Option>
              <Select.Option key="D1" value="D1">
                D1
              </Select.Option>
              <Select.Option key="BE" value="BE">
                BE
              </Select.Option>
              <Select.Option key="C1E" value="C1E">
                C1E
              </Select.Option>
              <Select.Option key="CE" value="CE">
                CE
              </Select.Option>
              <Select.Option key="D1E" value="D1E">
                D1E
              </Select.Option>
              <Select.Option key="DE" value="DE">
                DE
              </Select.Option>
            </Select>
          </FormItem>
          <FormItem
            key="imageUrl"
            label={getTooltip('Photo', 'Car Photo')}
            name="imageUrl"
            rules={[
              {
                required: false,
                message: `Please upload a photo`
              }
            ]}
          >
            <ImageUploadContainer>
              <ImageUpload
                onUpdate={value => {
                  setImageUrl(value)
                }}
              />
              <StyledAvatar size="96" shape="square" src={imageUrl} />
            </ImageUploadContainer>
          </FormItem>
        </Form>
      </Modal>
      {editUserCar && (
        <EditUserCar
          visible={display === 'edit'}
          onCancel={() => {
            setImageUrl(null)
            setDisplay('default')
            setEditUserCar(null)
            setSelectedBrand(null)
          }}
          title="Edit Car"
          editUserCar={editUserCar}
          onUpdate={handleUpdate}
          onDelete={handleDelete}
          imageUrl={imageUrl}
          selectedBrand={selectedBrand}
          setSelectedBrand={setSelectedBrand}
          setImageUrl={setImageUrl}
          cars={cars}
        />
      )}
    </>
  )
}

UserCars.propTypes = {
  userCars: PropTypes.arrayOf(PropTypes.object).isRequired,
  addUserCarHandler: PropTypes.func.isRequired,
  updateUserCarHandler: PropTypes.func.isRequired
}

export default UserCars
