import React from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, Form, Input, Select, Switch, Avatar } from 'antd'
import { CloseOutlined, CheckOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import { PlusOutlined } from '@ant-design/icons'

import ImageUpload from '../shared/ImageUpload'
import StyledTooltip from '../shared/StyledTooltip'
import showConfirm from '../shared/DeleteConfirm'

const { TextArea } = Input
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 }
  }
}

const ImageUploadContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledAvatar = styled(Avatar)`
  margin-left: 5px;
`

const EditUserCar = ({
  visible,
  onCancel,
  title,
  editUserCar,
  onUpdate,
  onDelete,
  imageUrl,
  setImageUrl,
  selectedBrand,
  setSelectedBrand,
  cars
}) => {
  const [form] = Form.useForm()

  const getTooltip = (text, fullText) => {
    return <StyledTooltip {...{ text, fullText }} />
  }

  return (
    <Modal
      visible={visible}
      title={title}
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancel}
      footer={[
        <Button
          danger
          style={{ float: 'left' }}
          onClick={() => {
            showConfirm(() => {
              onDelete(editUserCar.id)
            })
          }}
        >
          Delete
        </Button>,
        <Button key="submit" onClick={onCancel}>
          Cancel
        </Button>,
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                form.resetFields()
                onUpdate(editUserCar.id, values)
              })
              .catch(info => {
                console.log('Validate Failed:', info)
              })
          }}
        >
          Save
        </Button>
      ]}
    >
      <Form form={form} layout="vertical">
        <FormItem
          key="name"
          label={getTooltip('Car name', 'Car name')}
          name="name"
          rules={[
            {
              required: true,
              message: `Please write the Car Name`
            }
          ]}
          initialValue={editUserCar.name}
        >
          <Input placeholder="Name" />
        </FormItem>
        <FormItem
          key="brand"
          label={getTooltip('brand', `Car's brand`)}
          name="brand"
          rules={[
            {
              required: true,
              message: `Please write the Car brand`
            }
          ]}
          initialValue={editUserCar.brand}
        >
          <Select
            mode="single"
            placeholder="select brand"
            showSearch
            onChange={value =>
              setSelectedBrand(cars.find(car => car.brand === value))
            }
          >
            {cars.map(car => (
              <Select.Option key={car.brand} value={car.brand}>
                {car.brand}
              </Select.Option>
            ))}
          </Select>
        </FormItem>
        <FormItem
          key="model"
          label={getTooltip('Model', `Car's model`)}
          name="model"
          rules={[
            {
              required: true,
              message: `Please write the Car brand`
            }
          ]}
          initialValue={editUserCar.model}
        >
          <Select
            mode="single"
            placeholder="select brand"
            showSearch
            disabled={!selectedBrand}
          >
            {selectedBrand &&
              selectedBrand?.models.map(model => (
                <Select.Option key={model} value={model}>
                  {model}
                </Select.Option>
              ))}
          </Select>
        </FormItem>
        <FormItem
          key="year"
          label={getTooltip('Year of the Car', 'Year of the car')}
          name="year"
          initialValue={editUserCar.year}
          rules={[
            {
              required: true,
              message: `Please write Year of Car`
            }
          ]}
        >
          <Input type="number" placeholder="year" />
        </FormItem>
        <FormItem
          key="capacity"
          label={getTooltip('Car capacity', 'Car capacity')}
          name="capacity"
          rules={[
            {
              required: true,
              message: `Please write car Capacity`
            }
          ]}
          initialValue={editUserCar.capacity}
        >
          <Input type="number" placeholder="Capacity" />
        </FormItem>
        <FormItem
          key="type"
          label={getTooltip('Type ', 'Car Type')}
          name="type"
          rules={[
            {
              required: true,
              message: `Please write car Type`
            }
          ]}
          initialValue={editUserCar.type}
        >
          <Select mode="single" placeholder="select Type" showSearch>
            <Select.Option key="A1" value="A1">
              A1
            </Select.Option>
            <Select.Option key="A" value="A">
              A
            </Select.Option>
            <Select.Option key="B1" value="B1">
              B1
            </Select.Option>
            <Select.Option key="B" value="B">
              B
            </Select.Option>
            <Select.Option key="C1" value="C1">
              C1
            </Select.Option>
            <Select.Option key="C" value="C">
              C
            </Select.Option>
            <Select.Option key="D1" value="D1">
              D1
            </Select.Option>
            <Select.Option key="BE" value="BE">
              BE
            </Select.Option>
            <Select.Option key="C1E" value="C1E">
              C1E
            </Select.Option>
            <Select.Option key="CE" value="CE">
              CE
            </Select.Option>
            <Select.Option key="D1E" value="D1E">
              D1E
            </Select.Option>
            <Select.Option key="DE" value="DE">
              DE
            </Select.Option>
          </Select>
        </FormItem>
        <FormItem
          key="imageUrl"
          label={getTooltip('Photo', 'Car photo')}
          name="imageUrl"
          rules={[
            {
              required: false,
              message: `Please upload a photo`
            }
          ]}
        >
          <ImageUploadContainer>
            <ImageUpload
              onUpdate={value => {
                setImageUrl(value)
              }}
            />
            <StyledAvatar
              size="96"
              shape="square"
              src={imageUrl || editUserCar?.imageUrl}
            />
          </ImageUploadContainer>
        </FormItem>
      </Form>
    </Modal>
  )
}

EditUserCar.propTypes = {
  editUserCar: PropTypes.shape({}).isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default EditUserCar
