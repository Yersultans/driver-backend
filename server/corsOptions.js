import { AllowedUrls } from './envVariables'

const whitelist = AllowedUrls

const corsOptions = {
  preflightContinue: true,
  maxAge: 86400,
  origin(origin, callback) {
    // same origin
    if (!origin) return callback(null, true)

    if (Array.isArray(whitelist) && whitelist.includes(origin)) {
      return callback(null, true)
    }

    if (`${origin}`.localeCompare(`${whitelist}`)) return callback(null, true)

    return callback(new Error('Not allowed by CORS'))
  }
}

module.exports = corsOptions
