import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async userCars(_, args, ctx) {
      const items = await ctx.models.UserCar.find({ isDeleted: { $ne: true}}).sort({
        _id: -1
      })
      return items
    },
    async userCar(_, args, ctx) {
      const item = await ctx.models.UserCar.findById(args.id).exec()
      if (!item) {
        throw new Error('UserCar does not exist')
      }
      return item
    },
    async userCarsByUser(_, { userId }, ctx) {
      const items = await ctx.models.UserCar.find({ user: userId, isDeleted: { $ne: true}}).sort({
        _id: -1
      })
      return items
    },
  },
  Mutation: {
    async addUserCar(_, { input }, ctx) {
      const item = new ctx.models.UserCar(input)
      await item.save()
      return item
    },
    async updateUserCar(_, { id, input }, ctx) {
      const item = await ctx.models.UserCar.findOneAndUpdate({ _id: id }, input, {
        new: true
      }).exec()
      if (!item) {
        throw new Error('UserCar not found')
      }
      return item
    },
    async deleteUserCar(_, { id }, ctx) {
      const result = await ctx.models.UserCar.findOneAndUpdate({ _id: id }, { isDeleted: true })

      return id
    }
  },
  UserCar: {
    id(UserCar) {
      return `${UserCar._id}`
    }
  }
}
