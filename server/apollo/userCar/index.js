import model from './userCar.model'
import gqlLoader from '../gqlLoader'
import resolvers from './userCar.resolvers'
import loader from './userCar.loader'

module.exports = {
  typeDefs: gqlLoader('./userCar/userCar.graphql'),
  model,
  resolvers,
  loader
}
