const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const UserCarSchema = new Schema(
  {
    name: String,
		imageUrl: String,
    type: {
      type: String,
      enum: ['A1', 'A', 'B1', 'B', 'C1', 'C', 'D1', 'BE', 'C1E', 'CE', 'D1E', 'DE']
    },
    capacity: Number,
    brand: String,
    model: String,
    year: Number,
    user: { type: Schema.Types.ObjectId, ref: 'User' },
		isDeleted: Boolean
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

UserCarSchema.plugin(mongoosePaginate)

const UserCar = mongoose.model('UserCar', UserCarSchema)
module.exports = UserCar
