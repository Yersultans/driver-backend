import merge from 'lodash/merge'
import { PubSub } from 'apollo-server-express'
import { GraphQLScalarType } from 'graphql'
import passport from 'passport'
import auth from './auth'
import user from './user'
import card from './card'
import userCar from './userCar'
import order from './order'
import userMessage from './userMessage'

import gqlLoader from './gqlLoader'

require('./../passportHelper')(passport)

const pubsub = new PubSub()

const resolverMap = {
  DateTime: new GraphQLScalarType({
    name: 'DateTime',
    description: 'A date and time, represented as an ISO-8601 string',
    serialize: value => value.toISOString(),
    parseValue: value => new Date(value),
    parseLiteral: ast => new Date(ast.value)
  })
}

const serverConfig = {
  introspection: true,
  uploads: false,
  playground: true,
  tracing: true,
  typeDefs: [
    gqlLoader('./index.graphql'),
    user.typeDefs,
    auth.typeDefs,
    card.typeDefs,
    userCar.typeDefs,
    order.typeDefs,
    userMessage.typeDefs
  ].join(' '),
  resolvers: merge(
    {},
    auth.resolvers,
    user.resolvers,
    card.resolvers,
    userCar.resolvers,
    order.resolvers,
    userMessage.resolvers,
    resolverMap
  ),
  context: ({ req, connection }) => {
    return {
      user: connection ? null : req.user,
      logout: connection ? function emptyFunction() {} : req.logout,
      loaders: {
        userLoader: user.loader,
        cardLoader: card.loader,
        userCarLoader: userCar.loader,
        orderLoader: order.loader,
        userMessageLoader: userMessage.loader
      },
      models: {
        User: user.model,
        Card: card.model,
        UserCar: userCar.model,
        Order: order.model,
        UserMessage: userMessage.model
      },
      pubsub
    }
  }
}
module.exports = serverConfig
