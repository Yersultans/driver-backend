import jwt, { verify } from 'jsonwebtoken'
import { ApolloError } from 'apollo-server-express'
import { PASSPORT_KEY } from '../../envVariables'
import { authenticated, authorized } from './auth.helper'

module.exports = {
  Query: {
    getCurrentUser(_, __, ctx) {
      const { user } = ctx
      if (!user) {
        throw new Error('user is not authenticated')
      }
      return ctx.loaders.userLoader.load(user._id)
    }
  },
  Mutation: {
    async register(_, { input }, ctx) {
      try {
        const { password } = input
        const { User } = ctx.models
        delete input.password
        const user = await User.register(new User(input), password)

        const { user: userData } = await ctx.models.User.authenticate()(
          user.username,
          password
        )
        const token = jwt.sign(
          {
            role: userData.role,
            id: userData._id
          },
          PASSPORT_KEY
        )
        return { token }
      } catch (err) {
        if (err) {
          if (err.name === 'MongoError' && err.code === 11000) {
            // duplicate username or email
            throw new ApolloError('User already exists')
          }
          throw new ApolloError(err)
        }
      }
    },
    async loginAdmin(_, { input }, ctx) {
      const { username, password } = input
      const { user, error } = await ctx.models.User.authenticate()(
        username,
        password
      )
      if (!user) throw new Error(error)

      const adminRoles = ['admin']

      if (!adminRoles.includes(user.role)) {
        throw new Error('Only admin or contentManager can access')
      }
      const token = jwt.sign(
        { role: user.role, id: user._id, username: user.username },
        PASSPORT_KEY
      )
      return { token, user }
    },
    async loginUser(_, { input }, ctx) {
      const { username, password } = input

      const { user, error } = await ctx.models.User.authenticate()(
        username,
        password
      )
      if (!user) throw new Error(error)
      const token = jwt.sign(
        { role: user.role, id: user._id, username: user.username },
        PASSPORT_KEY
      )
      return { token }
    },
    logout(_, __, ctx) {
      try {
        ctx.logout()
        return { message: 'logout successful', status: 'ok' }
      } catch (err) {
        throw new Error(err)
      }
    }
  }
}
