import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async orders(_, args, ctx) {
      const items = await ctx.models.Order.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async order(_, args, ctx) {
      const item = await ctx.models.Order.findById(args.id).exec()
      if (!item) {
        throw new Error('Order does not exist')
      }
      return item
    },
    async ordersByStatus(_, { status }, ctx) {
      const items = await ctx.models.Order.find({
        status,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async activeOrder(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      const items = await ctx.models.Order.findOne({
        client: userId,
        $or: [
          { status: 'findedDriver' },
          { status: 'inProgress' },
          { status: 'findDriver' }
        ],
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async activeDriverOrder(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      const items = await ctx.models.Order.findOne({
        driver: userId,
        $or: [{ status: 'findedDriver' }, { status: 'inProgress' }],
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async userOrders(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      const items = await ctx.models.Order.find({
        $or: [{ client: userId }, { driver: userId }],
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    }
  },
  Mutation: {
    async addOrder(_, { input }, ctx) {
      const item = new ctx.models.Order({ ...input, status: 'findDriver' })
      await item.save()
      ctx.pubsub.publish('order', {
        order: {
          mutation: 'Added',
          data: item
        }
      })
      return item
    },
    async updateOrder(_, { id, input }, ctx) {
      const item = await ctx.models.Order.findOneAndUpdate({ _id: id }, input, {
        new: true
      }).exec()
      if (!item) {
        throw new Error('Order not found')
      }
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async deleteOrder(_, { id }, ctx) {
      const result = await ctx.models.Order.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )
      ctx.pubsub.publish('order', {
        order: {
          mutation: 'Deleted',
          data: result
        }
      })
      return id
    },
    async addDriverToOrder(_, { orderId, userId }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      item.status = 'findedDriver'
      item.driver = userId
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async startTrip(_, { orderId }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      item.status = 'inProgress'
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async driverCancelOrder(_, { orderId }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      item.status = 'findDriver'
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async finishOrder(_, { orderId }, ctx) {
      const item = await ctx.models.Order.findById(orderId)
      if (!item) {
        throw new Error('Order not found')
      }
      item.status = 'finished'
      await item.save()
      ctx.pubsub.publish('order', {
        message: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    }
  },
  Subscription: {
    order: {
      subscribe(_, args, { pubsub }) {
        return pubsub.asyncIterator('order')
      }
    }
  },
  Order: {
    id(order) {
      return `${order._id}`
    },
    async client(order, _, ctx) {
      const clientId = order.client
      if (clientId) {
        const client = await ctx.loaders.userLoader.load(clientId)
        return client
      }
      return null
    },
    async driver(order, _, ctx) {
      const driverId = order.driver
      if (driverId) {
        const driver = await ctx.loaders.userLoader.load(driverId)
        return driver
      }
      return null
    },
    async card(order, _, ctx) {
      const cardId = order.card
      if (cardId) {
        const card = await ctx.loaders.cardLoader.load(cardId)
        return card
      }
      return null
    }
  }
}
