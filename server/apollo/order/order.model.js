const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const OrderSchema = new Schema(
  {
    formAddress: {
      street: String,
      latitude: Number,
      longitude: Number
    },
    toAddress: {
      street: String,
      latitude: Number,
      longitude: Number
    },
    status: String,
    price: Number,
    paymentMethod: String,
    driver: { type: Schema.Types.ObjectId, ref: 'User' },
    client: { type: Schema.Types.ObjectId, ref: 'User' },
    card: { type: Schema.Types.ObjectId, ref: 'Card' },
    isDeleted: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

OrderSchema.plugin(mongoosePaginate)

const Order = mongoose.model('Order', OrderSchema)
module.exports = Order
