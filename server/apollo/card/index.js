import model from './card.model'
import gqlLoader from '../gqlLoader'
import resolvers from './card.resolvers'
import loader from './card.loader'

module.exports = {
  typeDefs: gqlLoader('./card/card.graphql'),
  model,
  resolvers,
  loader
}
