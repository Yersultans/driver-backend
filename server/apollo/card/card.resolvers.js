import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async cards(_, args, ctx) {
      const items = await ctx.models.Card.find({ isDeleted: { $ne: true}}).sort({
        _id: -1
      })
      return items
    },
    async card(_, args, ctx) {
      const item = await ctx.models.Card.findById(args.id).exec()
      if (!item) {
        throw new Error('Card does not exist')
      }
      return item
    },
    async userCards(_, { userId: userIdArg}, ctx) {
      const userId = userIdArg || ctx.user._id
      const items = await ctx.models.Card.find({ user: userId,  isDeleted: { $ne: true}}).sort({
        _id: -1
      })
      return items
    },
  },
  Mutation: {
    async addCard(_, { input }, ctx) {
      const item = new ctx.models.Card(input)
      await item.save()
      return item
    },
    async updateCard(_, { id, input }, ctx) {
      const item = await ctx.models.Card.findOneAndUpdate({ _id: id }, input, {
        new: true
      }).exec()
      if (!item) {
        throw new Error('Card not found')
      }
      return item
    },
    async deleteCard(_, { id }, ctx) {
      const result = await ctx.models.Card.findOneAndUpdate({ _id: id }, { isDeleted: true })

      return id
    }
  },
  Card: {
    id(Card) {
      return `${Card._id}`
    }
  }
}
