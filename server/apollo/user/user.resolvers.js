import { ApolloError } from 'apollo-server-express'
import nodemailer from 'nodemailer'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import path from 'path'
import mongoose from 'mongoose'
import { authenticated, authorized } from '../auth/auth.helper'

export default {
  Query: {
    users: authenticated(async (_, args, ctx) => {
      const users = await ctx.models.User.find({
        isDeleted: { $ne: true }
      }).sort({
        created_at: -1
      })
      return users
    }),
    findUsers: authenticated(async (_, args, ctx) => {
      try {
        const { role } = args.queryUsers || {}
        const { page: queryPage, limit: queryLimit } = args.pagination || {}

        const pageSizeLimit = Number(queryLimit)
        const queryOptions = Object.assign(
          {},
          queryLimit && { limit: pageSizeLimit || 10 },
          queryPage && { page: queryPage || 0 }
        )

        const searchOptions = Object.assign(
          {},
          phoneNumber && {
            phoneNumber: { $regex: new RegExp(`^${phoneNumber}`, 'i') }
          },
          role && { role },
          isDeleted && true
        )
        const {
          docs: findedUsers,
          total,
          limit,
          page,
          pages
        } = await ctx.models.User.paginate(searchOptions, queryOptions)

        const users = role
          ? findedUsers.filter(user => user.role === role)
          : findedUsers
        return { users, total, limit, page, pages }
      } catch (err) {
        console.log('error_: ', err)
        return err
        // res.status(500).send({ err })
      }
    }),
    async user(_, args, ctx) {
      const item = await ctx.models.User.findById(args.id).exec()

      if (!item) {
        throw new Error('User does not exist')
      }
      return item
    },
    usersByRole: authenticated(async (_, { role }, ctx) => {
      const users = await ctx.models.User.find({ role, isDeleted: false })
      return users
    })
  },
  Mutation: {
    async addUser(_, { input }, ctx) {
      try {
        const { User } = ctx.models
        const { password } = input
        delete input.password
        const user = await User.register(new User(input), `${password}`)

        await user.save()

        return user
      } catch (err) {
        if (err) {
          if (err.name === 'MongoError' && err.code === 11000) {
            // duplicate username or email
            throw new ApolloError('User already exists')
          }
          throw new ApolloError(err)
        }
      }
    },
    updateUser: authenticated(async (_, { id, input }, ctx) => {
      try {
        const { password } = input
        delete input.password
        const user = await ctx.models.User.findOneAndUpdate(
          { _id: id },
          input,
          {
            new: true
          }
        ).exec()
        if (!user) {
          throw new Error('User not found')
        }
        if (password) {
          const sanitizedUser = await user.setPassword(password)
          await sanitizedUser.save()
        }
        return user
      } catch (err) {
        if (err) {
          if (err.name === 'MongoError' && err.code === 11000) {
            // duplicate phoneNumber
            throw new ApolloError('User not updated')
          }

          // Some other error
          throw new ApolloError(err)
        }
      }
    }),
    deleteUser: authenticated(
      authorized(['admin'], async (_, { id }, ctx) => {
        try {
          const result = await ctx.models.User.deleteOne({ _id: id })

          if (result.deletedCount !== 1) {
            throw new Error('User not deleted')
          }
          return id
        } catch (err) {
          throw new ApolloError(err)
        }
      })
    ),
    editUser: authenticated(async (_, { id, input }, ctx) => {
      try {
        const { password } = input
        delete input.password
        const user = await ctx.models.User.findOneAndUpdate(
          { _id: id },
          input,
          {
            new: true
          }
        ).exec()
        if (!user) {
          throw new Error('User not found')
        }
        if (password) {
          const sanitizedUser = await user.setPassword(password)
          await sanitizedUser.save()
        }
        return user
      } catch (err) {
        if (err) {
          if (err.name === 'MongoError' && err.code === 11000) {
            // duplicate phoneNumber
            throw new ApolloError('User not updated')
          }

          // Some other error
          throw new ApolloError(err)
        }
      }
    })
  },
  User: {
    id(user) {
      return `${user._id}`
    },
    async car(user, _, ctx) {
      const userId = user._id
      if (userId) {
        const car = await ctx.models.Car.findOne({ user: userId })
        return car
      }
      return null
    }
    // async cards(user, _, ctx) {
    //   if (!user.cards) return []
    //   const cards = await ctx.loaders.cardLoader.loadMany(
    //     user.cards.filter(program => program != null)
    //   )
    //   return cards
    // },
    // async defaultCard(user, _, ctx) {
    //   const defaultCardId = user.defaultCard
    //   if (defaultCardId) {
    //     const defaultCard = await ctx.loaders.cardLoader.load(defaultCardId)
    //     return defaultCard
    //   }
    //   return null
    // }
  }
}
