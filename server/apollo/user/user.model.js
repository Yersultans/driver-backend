const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose
const passportLocalMongoose = require('passport-local-mongoose')

const UserSchema = new Schema(
  {
    username: { type: String, required: true, index: true, unique: true },
    lastname: String,
    firstname: String,
    birthday: Date,
    role: {
      type: String,
      required: true,
      enum: ['user', 'admin', 'driver'],
      default: 'user'
    },
    gender: {
      type: String,
      enum: ['male', 'female']
    },
    avatarUrl: String,
    defaultCard: { type: Schema.Types.ObjectId, ref: 'Card' },
    cards: [{ type: Schema.Types.ObjectId, ref: 'Card' }],
    phoneNumber: { type: String },
    isDeleted: {
      type: Boolean,
      default: false
    },
    isBlocked: {
      type: Boolean,
      default: false
    }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

UserSchema.plugin(passportLocalMongoose, {
  usernameField: 'username'
})
UserSchema.plugin(mongoosePaginate)

const User = mongoose.model('User', UserSchema)
module.exports = User
