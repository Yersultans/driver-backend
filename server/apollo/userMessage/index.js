import model from './userMessage.model'
import gqlLoader from '../gqlLoader'
import resolvers from './userMessage.resolvers'
import loader from './userMessage.loader'

module.exports = {
  typeDefs: gqlLoader('./userMessage/userMessage.graphql'),
  model,
  resolvers,
  loader
}
