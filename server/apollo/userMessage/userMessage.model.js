const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const { Schema } = mongoose

const UserMessageSchema = new Schema(
  {
    text: String,
    isReaded: {
      type: Boolean,
      default: false
    },
    fromUser: { type: Schema.Types.ObjectId, ref: 'User' },
    toUser: { type: Schema.Types.ObjectId, ref: 'User' },
    order: { type: Schema.Types.ObjectId, ref: 'Order' }
  },
  { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
)

UserMessageSchema.plugin(mongoosePaginate)

const UserMessage = mongoose.model('UserMessage', UserMessageSchema)
module.exports = UserMessage
