import { authenticated, authorized } from '../auth/auth.helper'

module.exports = {
  Query: {
    async userMessages(_, args, ctx) {
      const items = await ctx.models.UserMessage.find({
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    },
    async userMessage(_, args, ctx) {
      const item = await ctx.models.UserMessage.findById(args.id).exec()
      if (!item) {
        throw new Error('UserMessage does not exist')
      }
      return item
    },
    async unreadUserMessage(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      const order = await ctx.models.Order.findOne({
        client: userId,
        $or: [
          { status: 'findedDriver' },
          { status: 'inProgress' },
          { status: 'findDriver' }
        ],
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      if (order) {
        await ctx.models.UserMessage.find({
          order: item.id,
          isReaded: false,
          isDeleted: { $ne: true }
        }).sort({
          _id: -1
        })
      }

      return items
    },
    async messagesByOrderId(_, { orderId }, ctx) {
      const items = await ctx.models.UserMessage.find({
        order: orderId,
        isDeleted: { $ne: true }
      }).sort({
        _id: -1
      })
      return items
    }
  },
  Mutation: {
    async addUserMessage(_, { input }, ctx) {
      const item = new ctx.models.UserMessage(input)
      await item.save()
      ctx.pubsub.publish('userMessage', {
        userMessage: {
          mutation: 'Added',
          data: item
        }
      })
      return item
    },
    async updateUserMessage(_, { id, input }, ctx) {
      const item = await ctx.models.UserMessage.findOneAndUpdate(
        { _id: id },
        input,
        {
          new: true
        }
      ).exec()
      if (!item) {
        throw new Error('UserMessage not found')
      }
      ctx.pubsub.publish('userMessage', {
        userMessage: {
          mutation: 'Updated',
          data: item
        }
      })
      return item
    },
    async deleteUserMessage(_, { id }, ctx) {
      const result = await ctx.models.UserMessage.findOneAndUpdate(
        { _id: id },
        { isDeleted: true }
      )
      ctx.pubsub.publish('userMessage', {
        userMessage: {
          mutation: 'Deleted',
          data: result
        }
      })
      return id
    },
    async readAllMessages(_, { userId: userIdArg }, ctx) {
      const userId = userIdArg || ctx.user._id
      const result = await ctx.models.UserMessage.updateMany(
        { toUser: userId, isReaded: false },
        { isReaded: true }
      )
      return { message: 'send messages', status: 'ok' }
    }
  },
  Subscription: {
    userMessage: {
      subscribe(_, args, { pubsub }) {
        return pubsub.asyncIterator('userMessage')
      }
    }
  },
  UserMessage: {
    id(userMessage) {
      return `${userMessage._id}`
    },
    async fromUser(userMessage, _, ctx) {
      const userId = userMessage.fromUser
      if (userId) {
        const user = await ctx.loaders.userLoader.load(userId)
        return user
      }
      return null
    },
    async toUser(userMessage, _, ctx) {
      const userId = userMessage.toUser
      if (userId) {
        const user = await ctx.loaders.userLoader.load(userId)
        return user
      }
      return null
    },
    async order(userMessage, _, ctx) {
      const orderId = userMessage.order
      if (orderId) {
        const order = await ctx.loaders.orderLoader.load(orderId)
        return order
      }
      return null
    }
  }
}
