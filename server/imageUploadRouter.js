import * as fbAdmin from 'firebase-admin'
import { v4 as uuidv4 } from 'uuid'
import { format } from 'util'
import Multer from 'multer'
import fs from 'fs'
import compressImages from 'compress-images'
import serviceAccount from './firebaseCredentials'
import { FIREBASE_STORAGE_BUCKET } from './envVariables'

// image upload
fbAdmin.initializeApp({
  credential: fbAdmin.credential.cert(serviceAccount),
  storageBucket: FIREBASE_STORAGE_BUCKET
})

const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 20 * 1024 * 1024 // no larger than 20mb, you can change as needed.
  }
})

const bucket = fbAdmin.storage().bucket()

/**
 * Upload the image file to Google Storage
 * @param {File} file object that will be uploaded to Google Storage
 */

const uploadImageToFirebase = file => {
  return new Promise((resolve, reject) => {
    if (!file) {
      reject('No image file')
    }
    const newFileName = `_${Date.now()}_${file.originalname}`

    const fileUpload = bucket.file(newFileName)
    const uuid = uuidv4()
    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype,
        metadata: { firebaseStorageDownloadTokens: uuid }
      }
    })

    blobStream.on('error', error => {
      reject('Something is wrong! Unable to upload at the moment.', error)
    })

    blobStream.on('finish', () => {
      // The public URL can be used to directly access the file via HTTP.
      const url = format(
        `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${fileUpload.name}?alt=media&token=${uuid}`
      )
      resolve(url)
    })

    blobStream.end(file.buffer)
  })
}

export const uploadImageWithCompressed = app => {
  app.post(
    '/api/uploadImageWithCompressed',
    multer.single('image'),
    async (req, res) => {
      const { file } = req
      const { originalname, buffer } = file
      const directoryPrefix = './server/routes/photoCompression'

      await fs.writeFile(
        `${directoryPrefix}/inputImages/${originalname}`,
        buffer,
        err => {
          if (err) throw err
          const inputPath = `${directoryPrefix}/inputImages/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}`
          const outputPath = `${directoryPrefix}/outputImages/`
          compressImages(
            inputPath,
            outputPath,
            { compress_force: false, statistic: true, autoupdate: true },
            false,
            { jpg: { engine: 'mozjpeg', command: ['-quality', '60'] } },
            { png: { engine: 'pngquant', command: ['--quality=20-50', '-o'] } },
            { svg: { engine: 'svgo', command: '--multipass' } },
            {
              gif: {
                engine: 'gifsicle',
                command: ['--colors', '64', '--use-col=web']
              }
            },
            async (error, completed) => {
              if (completed) {
                // compression completed
                // read compressed files into memory
                fs.readFile(
                  `${directoryPrefix}/outputImages/${originalname}`,
                  async (err1, data) => {
                    if (err1) throw err1
                    // upload both original and compressed
                    const url = await uploadImageToFirebase(file)
                    const compressedUrl = await uploadImageToFirebase({
                      ...file,
                      buffer: data
                    })
                    console.log({
                      url,
                      compressedUrl
                    })

                    // send two urls
                    res.status(200).send({
                      success: 1,
                      file: { url },
                      compressedFile: {
                        url: compressedUrl
                      }
                    })

                    // clean input images directory
                    fs.readdir(
                      `${directoryPrefix}/inputImages`,
                      (err2, files) => {
                        if (err2) throw err2

                        files.forEach(fileInDirectory => {
                          fs.unlink(
                            `${directoryPrefix}/inputImages/${fileInDirectory}`,
                            err3 => {
                              if (err3) throw err3
                            }
                          )
                        })
                      }
                    )
                    // clean outputImages directory
                    fs.readdir(
                      `${directoryPrefix}/outputImages`,
                      (err4, files) => {
                        if (err4) throw err4

                        files.forEach(fileInDirectory => {
                          fs.unlink(
                            `${directoryPrefix}/outputImages/${fileInDirectory}`,
                            err5 => {
                              if (err5) throw err5
                            }
                          )
                        })
                      }
                    )
                    // clear logs
                    fs.readdir(
                      `${directoryPrefix}/log/compress-images`,
                      (err6, files) => {
                        if (err6) throw err6

                        files.forEach(fileInDirectory => {
                          fs.unlink(
                            `${directoryPrefix}/log/compress-images/${fileInDirectory}`,
                            err7 => {
                              if (err7) throw err7
                            }
                          )
                        })
                      }
                    )
                  }
                )

                // })
              }
            }
          )
        }
      )
      console.log('resolved promise')
    }
  )
}

export const uploadImageByFile = app => {
  app.post(
    '/api/uploadImageByFile',
    multer.single('image'),
    async (req, res) => {
      const { file, fileList } = req
      if (file) {
        uploadImageToFirebase(file)
          .then(url => {
            res.status(200).send({
              success: 1,
              file: { url }
            })
          })
          .catch(error => {
            console.error(error)
          })
      }
    }
  )
}
