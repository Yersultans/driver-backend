import express from 'express'
import mongoose from 'mongoose'
import session from 'express-session'
import connectMongo from 'connect-mongo'
import path from 'path'
import secure from 'express-force-https'
import cors from 'cors'
import expressValidator from 'express-validator'
import passport from 'passport'
import bodyParser from 'body-parser'
import sslRedirect from 'heroku-ssl-redirect'
// const logger = require('morgan');

// import corsOptions from './corsOptions'
import {
  uploadImageByFile,
  uploadImageWithCompressed
} from './imageUploadRouter'
import apollo from './apollo'
import fetchUrlRoutes from './fetchUrlRoutes'
import { NODE_ENV, MONGO_URL } from './envVariables'

const app = express()
const MongoStore = connectMongo(session)
const mongoUrl = MONGO_URL
mongoose.connect(mongoUrl, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
})

// app.use(logger('common'));
app.use(sslRedirect())
app.use(cors())

app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(expressValidator())
app.use(
  session({
    secret: 'sdfkjasklfdhakjlsfhksad',
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: { secure: false }
  })
)
app.use(secure)
// authorization
require('./passportHelper')(passport)

app.use(passport.initialize())
app.use(passport.session())

app.use('/graphql', (req, res, next) => {
  passport.authenticate('jwt', { session: false }, function addUser(err, user) {
    if (user) {
      req.user = user
    }
    if (err) {
      return next(err)
    }
    return next()
  })(req, res, next)
})

const { httpServer, server } = apollo(app)
fetchUrlRoutes(app)
uploadImageByFile(app)
uploadImageWithCompressed(app)

/** ********************* Configure static files ********************** */
if (NODE_ENV === 'production' || NODE_ENV === 'staging') {
  /* Admin */
  const adminPath = path.join(__dirname, '..', 'admin', 'build')
  const adminPathIndexFile = path.join(
    __dirname,
    '..',
    'admin',
    'build',
    'index.html'
  )
  app.use('/public/', express.static(adminPath))
  app.get('/login', (_, res) => res.sendFile(adminPathIndexFile))
  app.get('/register', (_, res) => res.sendFile(adminPathIndexFile))
  app.get('/', (_, res) => res.sendFile(adminPathIndexFile))
  app.get('/*', (_, res) => res.sendFile(adminPathIndexFile))
}

const PORT = process.env.PORT || 5001

httpServer.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(
    `🚀 Server 1 ready at http://localhost:${PORT}${server.graphqlPath}`
  )
  console.log(
    `🚀 Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`
  )
})

export default app
